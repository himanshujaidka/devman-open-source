import React, { Fragment } from "react";
import { Typography } from "@material-ui/core";

// const content = (
//   <Fragment>
    {/* <Typography variant="h6" paragraph>
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
      eirmod tempor invidunt ut labore.
    </Typography> */}
    {/* <Typography paragraph>
      The objective of the DailyMint is to provide real and genuine news to readers. 
      You are invited to contribute and add more features to it.
    </Typography>
    <Typography paragraph>
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
      eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
      voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
      clita kasd gubergren, no sea takimata sanctus est Lorem.
    </Typography>
    <Typography variant="h6" paragraph>
      Title
    </Typography>
    <Typography paragraph>
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
      eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
      voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
      clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
      amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
      nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed
      diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
      Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor
      sit amet.
    </Typography>
    <Typography paragraph>
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
      eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
      voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
      clita kasd gubergren, no sea takimata sanctus est Lorem.
    </Typography>
    <Typography paragraph>
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
      eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
      voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
      clita kasd gubergren, no sea takimata sanctus est Lorem.
    </Typography>
    <Typography paragraph>
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
      eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
      voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
      clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
      amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
      nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed
      diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
      Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor
      sit amet.
    </Typography>
    <Typography variant="h6" paragraph>
      Title
    </Typography>
    <Typography paragraph>
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
      eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
      voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
      clita kasd gubergren, no sea takimata sanctus est Lorem.
    </Typography>
    <Typography paragraph>
      Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
      eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
      voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
      clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
      amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
      nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed
      diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
      Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor
      sit amet.
    </Typography>
  </Fragment>
); */}

const posts = [
  {
    title: <a href="https://github.com/DevMan-open-source/dailymint" target="__blank"style={{ textDecoration: 'none', color:'#35c4a7' }}>Daily Mint</a>,
    id: 1,
    date: 1576281600,
    src: `${process.env.PUBLIC_URL}/images/logged_out/dailyMint.png`,
    snippet:
      "News Project. Objective is to providing real and genuine news to readers. Tools used- Html, CSS, Bootstrap4, Javascript. Future goals: introduce backend using node.js and use of nosql database like: mongodb. Redesign frontend using react.js",
    // content: "https://github.com/DevMan-open-source/dailymint",
  },
  {
    title: <a href="https://github.com/DevMan-open-source/ecommerce-store" target="__blank"style={{ textDecoration: 'none', color:'#35c4a7' }}>E-Commerce</a>,
    id: 2,
    date: 1576391600,
    src: `${process.env.PUBLIC_URL}/images/logged_out/ecommerce.jpg`,
    snippet:
      "Simple and easy to use ecommerce store that uses java/kotlin and firebase technology. Future goals: once it is ready then we will launch it to the google play store.",
    // content: content,
  },
  {
    title: <a href="https://github.com/DevMan-open-source/Relaxitori" target="__blank"style={{ textDecoration: 'none', color:'#35c4a7' }}>Relaxitory</a> ,
    id: 3,
    date: 1577391600,
    src: `${process.env.PUBLIC_URL}/images/logged_out/logoR.png`,
    snippet:
      "Relaxitory is a zone for people to come and relax, listen to music, and understand how important is mental health. It just gets you into a very calm and peaceful world, where all you have to do is Relax and Listen to the music. Future goals: frontend is developing using react.js and for the backend we will keep node.js",
    // content: content,
  },
  {
    title: <a href="https://github.com/DevMan-open-source/NotesApp" target="__blank"style={{ textDecoration: 'none', color:'#35c4a7' }}>NotesApp</a>,
    id: 4,
    date: 1572281600,
    src: `${process.env.PUBLIC_URL}/images/logged_out/notesapp.png`,
    snippet:
      "A simple note taking app that uses java/kotlin and sql technology. Future Goals: working on the more features, you are open to give your suggesstions.",
    // content: content,
  // },
  // {
  //   title: "Post 5",
  //   id: 5,
  //   date: 1573281600,
  //   src: `${process.env.PUBLIC_URL}/images/logged_out/blogPost5.jpg`,
  //   snippet:
  //     "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.",
  //    content: content,
  // },
  // {
  //   title: "Post 6",
  //   id: 6,
  //   date: 1575281600,
  //   src: `${process.env.PUBLIC_URL}/images/logged_out/blogPost6.jpg`,
  //   snippet:
  //     "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.",
  //    content: content,
   },
];

export default posts;