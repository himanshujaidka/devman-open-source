import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import {
  Grid,
  Typography,
  isWidthUp,
  withWidth,
  withStyles
} from "@material-ui/core";
import PriceCard from "./PriceCard";
import calculateSpacing from "./calculateSpacing";

const styles = theme => ({
  containerFix: {
    [theme.breakpoints.down("md")]: {
      paddingLeft: theme.spacing(6),
      paddingRight: theme.spacing(6)
    },
    [theme.breakpoints.down("sm")]: {
      paddingLeft: theme.spacing(4),
      paddingRight: theme.spacing(4)
    },
    [theme.breakpoints.down("xs")]: {
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2)
    },
    overflow: "hidden",
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1)
  },
  cardWrapper: {
    [theme.breakpoints.down("xs")]: {
      marginLeft: "auto",
      marginRight: "auto",
      maxWidth: 340
    }
  },
  cardWrapperHighlighted: {
    [theme.breakpoints.down("xs")]: {
      marginLeft: "auto",
      marginRight: "auto",
      maxWidth: 360
    }
  }
});

function PricingSection(props) {
  const { width, classes } = props;
  return (
    <div className="lg-p-top" style={{ backgroundColor: "#FFFFFF" }}>
      <Typography variant="h3" align="center" className="lg-mg-bottom">
        Open Projects
      </Typography>
      <div className={classNames("container-fluid", classes.containerFix)}>
        <Grid
          container
          spacing={calculateSpacing(width)}
          className={classes.gridContainer}
        >
          <Grid
            item
            xs={12}
            sm={6}
            lg={3}
            className={classes.cardWrapper}
            data-aos="zoom-in-up"
          >
            <PriceCard
              title="Web Development"
              pricing={
                <a href="https://github.com/DevMan-open-source/dailymint" style={{ textDecoration: 'none', color:'black' }}><span>
                  Daily Mint
                </span></a>
              }
              features={["providing the genuine news", "using MERN stack", "explore open issues"]}
            />
          </Grid>
          <Grid
            item
            className={classes.cardWrapper}//Highlighted
            xs={12}
            sm={6}
            lg={3}
            data-aos="zoom-in-up"
            data-aos-delay="200"
          >
            <PriceCard
              // highlighted
              title="App Development"
              pricing={
                <a href="https://github.com/DevMan-open-source/ecommerce-store" style={{ textDecoration: 'none', color:'black' }}>
                  <span>
                  E-Commerce
                  {/* <Typography display="inline"> / month</Typography> */}
                </span>
                </a>
              }
              features={["ecommerce shopping app", "using java/kotlin and firebase", "explore open issues"]}
            />
          </Grid>
          <Grid
            item
            className={classes.cardWrapper}
            xs={12}
            sm={6}
            lg={3}
            data-aos="zoom-in-up"
            data-aos-delay={isWidthUp("md", width) ? "400" : "0"}
          >
            <PriceCard
              title="Web Development"
              pricing={
                 <a href="https://github.com/DevMan-open-source/Relaxitori" style={{ textDecoration: 'none', color:'black' }}><span>
                 Relaxitory
                  {/* <Typography display="inline"> / month</Typography> */}
                </span></a>
              }
              features={["web app for mental health & fitness", "using MERN stack", "explore open issues"]}
            />
          </Grid>
          <Grid
            item
            className={classes.cardWrapper}
            xs={12}
            sm={6}
            lg={3}
            data-aos="zoom-in-up"
            data-aos-delay={isWidthUp("md", width) ? "600" : "200"}
          >
            <PriceCard
              title="App development"
              pricing={
               <a href="https://github.com/DevMan-open-source/NotesApp" style={{ textDecoration: 'none', color:'black' }}><span>
                  Notes App
                  {/* <Typography display="inline"> / month</Typography> */}
                </span></a>
              }
              features={["Helps to take notes", "using java and sql", "explore open issues"]}
            />
          </Grid>
        </Grid>
      </div>
    </div>
  );
}

PricingSection.propTypes = {
  width: PropTypes.string.isRequired
};

export default withStyles(styles, { withTheme: true })(
  withWidth()(PricingSection)
);
